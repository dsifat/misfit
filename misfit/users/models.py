from django.contrib.auth.models import AbstractUser
from django.db import models
from django.db.models import CharField
from django.urls import reverse
from django.utils.translation import ugettext_lazy as _

from config.settings.base import AUTH_USER_MODEL
from misfit.users.config import RequestStatus


class User(AbstractUser):
    name = CharField(_("Name of User"), blank=True, max_length=255)
    is_employee = models.BooleanField(default=True)

    def get_absolute_url(self):
        return reverse("users:detail", kwargs={"username": self.username})


class DRequest(models.Model):
    description = models.CharField(max_length=500)
    creator = models.ForeignKey(AUTH_USER_MODEL,
                                related_name="%(class)s_created_by",
                                on_delete=models.CASCADE)

    updated_by = models.ForeignKey(AUTH_USER_MODEL, blank=True, null=True,
                                   related_name="%(class)s_updated_by", on_delete=models.SET_NULL)

    status = models.IntegerField(choices=RequestStatus.CHOICES, null=True, blank=True, default=RequestStatus.CHOICES[0][0])

    def __str__(self):
        return f'{self.creator}\'s Request'

    def get_absolute_url(self):
        return reverse("request:detail",kwargs={"id":self.id})
