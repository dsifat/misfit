from django.urls import path

from misfit.users.views import (
    user_redirect_view,
    user_update_view,
    user_detail_view,
    request_list_view, all_requests)

app_name = "users"
urlpatterns = [
    path("~redirect/", view=user_redirect_view, name="redirect"),
    path("~update/", view=user_update_view, name="update"),
    # path("~requests/", view=user_update_view, name="update"),
    path("<str:username>/", view=user_detail_view, name="detail"),
]

