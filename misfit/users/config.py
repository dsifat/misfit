class RequestStatus:

    PENDING = 1
    HRVIEWED = 2
    APPROVED = 3

    CHOICES = (
        (PENDING, "Pending"),
        (HRVIEWED, "HR Reviewed"),
    )
    SU_CHOICES = (
        (PENDING, "Pending"),
        (HRVIEWED, "HR Reviewed"),
        (APPROVED, "Approved"),
    )

