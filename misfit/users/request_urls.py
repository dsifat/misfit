from django.urls import path

from misfit.users.views import (

    request_list_view, user_request_create, user_request_list, user_request_detail, user_request_update, all_requests)

app_name = "request"
urlpatterns = [
    path("", view=user_request_list, name="list"),
    path("create/", view=user_request_create, name="create"),
    path("<int:id>/", view=user_request_detail, name="detail"),
    path("<int:id>/update/", view=user_request_update, name="update"),
]
