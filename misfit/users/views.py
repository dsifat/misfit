from django.contrib.auth import get_user_model
from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import get_object_or_404
from django.urls import reverse
from django.views.generic import DetailView, RedirectView, UpdateView, ListView, CreateView

from misfit.users.forms import UserRequestForm
from misfit.users.models import DRequest

User = get_user_model()


class UserDetailView(LoginRequiredMixin, DetailView):
    model = User
    slug_field = "username"
    slug_url_kwarg = "username"


user_detail_view = UserDetailView.as_view()


class UserUpdateView(LoginRequiredMixin, UpdateView):
    model = User
    fields = ["name"]

    def get_success_url(self):
        return reverse("users:detail", kwargs={"username": self.request.user.username})

    def get_object(self):
        return User.objects.get(username=self.request.user.username)


user_update_view = UserUpdateView.as_view()


class UserRedirectView(LoginRequiredMixin, RedirectView):

    permanent = False

    def get_redirect_url(self):
        return reverse("users:detail", kwargs={"username": self.request.user.username})

user_redirect_view = UserRedirectView.as_view()

class AllRequestList(LoginRequiredMixin, ListView):
    model = DRequest
    paginate_by = 10
    template_name = "request/requests_list.html"

    def get_queryset(self):
        return DRequest.objects.all()

all_requests = AllRequestList.as_view()

class UserRequestList(LoginRequiredMixin, ListView):
    model = DRequest
    paginate_by = 10
    template_name = "users/user_request.html"

    def get_queryset(self):
        return DRequest.objects.filter(creator=self.request.user)


user_request_list = UserRequestList.as_view()


class UserRequestCreate(LoginRequiredMixin, CreateView):
    model = DRequest
    form_class = UserRequestForm
    template_name = "users/user_request_form.html"

    def get_form_kwargs(self):
        kwargs = super(UserRequestCreate,self).get_form_kwargs()
        kwargs.update({"request":self.request})
        return kwargs

    def get_success_url(self):
        return reverse("request:list")

    def form_valid(self, form):
        form.instance.creator = self.request.user
        return super().form_valid(form)


user_request_create = UserRequestCreate.as_view()


class UserRequestDetail(LoginRequiredMixin, DetailView):
    model = DRequest
    slug_field = "id"
    slug_url_kwarg = "id"
    template_name = "users/user_request_detail.html"


user_request_detail = UserRequestDetail.as_view()


class UserRequestUpdate(LoginRequiredMixin, UpdateView):
    model = DRequest
    form_class = UserRequestForm
    template_name = "users/user_request_form.html"
    #
    # def get_success_url(self):
    #     return reverse("request:detail", kwargs={"id":self.kwargs.get('id')})

    def get_form_kwargs(self):
        kwargs = super(UserRequestUpdate,self).get_form_kwargs()
        kwargs.update({"request":self.request})
        return kwargs

    def get_object(self):
        id_=self.kwargs.get('id')
        return get_object_or_404(DRequest, id=id_)

    def form_valid(self, form):
        form.instance.updated_by = self.request.user
        return super().form_valid(form)


user_request_update = UserRequestUpdate.as_view()


class RequestListView(LoginRequiredMixin, ListView):
    model = DRequest
    paginate_by = 10
    template_name = 'request/requests_list.html'


request_list_view = RequestListView.as_view()
