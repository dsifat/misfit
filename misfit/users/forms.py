from django.contrib.auth import get_user_model, forms
from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _
from django import forms as form

from misfit.users.config import RequestStatus
from misfit.users.models import DRequest

User = get_user_model()


class UserChangeForm(forms.UserChangeForm):
    class Meta(forms.UserChangeForm.Meta):
        model = User


class UserCreationForm(forms.UserCreationForm):
    error_message = forms.UserCreationForm.error_messages.update(
        {"duplicate_username": _("This username has already been taken.")}
    )

    class Meta(forms.UserCreationForm.Meta):
        model = User

    def clean_username(self):
        username = self.cleaned_data["username"]

        try:
            User.objects.get(username=username)
        except User.DoesNotExist:
            return username

        raise ValidationError(self.error_messages["duplicate_username"])


class UserRequestForm(form.ModelForm):

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop('request', None)
        user = self.request.user
        super(UserRequestForm, self).__init__(*args, **kwargs)

        if user.is_superuser:
            self.fields['status'].choices = RequestStatus.SU_CHOICES
        elif user.is_staff:
            self.fields['status'].choices = RequestStatus.CHOICES
        else:
            self.fields['status'].widget.attrs['disabled'] = True

    class Meta:
        model = DRequest
        fields = ['description','status']
