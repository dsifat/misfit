Setting Up Development Environment
----------------------------------

Make sure to have the following on your host:

* Python 3.6
* PostgreSQL

First things first.

#. Create a virtualenv: ::

    $ python3.6 -m venv <virtual env path>

#. Activate the virtualenv you have just created: ::

    $ source <virtual env path>/bin/activate

#. Install development requirements: ::

    $ pip install -r requirements/local.txt

#. Create a new PostgreSQL Database.

#. Create a .env file in root directory and write your credentials: ::

    DATABASE_URL=postgres://<DB_NAME>:<DB_PASSWORD>@127.0.0.1:5432/misfit
    CELERY_BROKER_URL=redis://localhost:6379/0
    DJANGO_READ_DOT_ENV_FILE=True

#. Apply migrations: ::

    $ python manage.py migrate

#. See the application being served through Django development server: ::

    $ python manage.py runserver 0.0.0.0:8000

#. I used Pytest for Unit Testing. Just write pytest ::

       $ pytest

.. _PostgreSQL: https://www.postgresql.org/download/
.. _Redis: https://redis.io/download
.. _createdb: https://www.postgresql.org/docs/current/static/app-createdb.html
.. _initial PostgreSQL set up: http://suite.opengeo.org/docs/latest/dataadmin/pgGettingStarted/firstconnect.html
.. _postgres documentation: https://www.postgresql.org/docs/current/static/auth-pg-hba-conf.html
.. _direnv: https://direnv.net/
